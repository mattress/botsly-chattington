// Package config contains objects that parse and load config from yaml files
package config

import "errors"
import log "github.com/sirupsen/logrus"
import "unicode/utf8"

var logFatalf = log.Fatalf

// Config struct contains coniguration values parsed from the yaml config file
type Config struct {
	DevTeam        string `yaml:"DevTeam,omitempty"`
	DevChannel     string `yaml:"DevChannel,omitempty"`
	LogFile        string `yaml:"LogFile,omitempty"`
	KarmaStoreFile string `yaml:"KarmaStoreFile,omitempty"`
	GiphyAPIKey    string `yaml:"GiphyAPIKey,omitempty"`
	GiphyDLDir     string `yaml:"GiphyDLDir,omitempty"`
}

func (c *Config) validate() error {
	if utf8.RuneCountInString(c.DevTeam) == 0 {
		return errors.New("Missing DevTeam config value")
	}
	if utf8.RuneCountInString(c.DevChannel) == 0 {
		return errors.New("Missing DevChannel config value")
	}
	if utf8.RuneCountInString(c.KarmaStoreFile) == 0 {
		return errors.New("Missing KarmaStoreFile config value")
	}
	if utf8.RuneCountInString(c.LogFile) == 0 {
		return errors.New("Missing LogFile config value")
	}
	return nil
}

// Validate confirms whether all required config fields have been set
func (c *Config) Validate() {
	if err := c.validate(); err != nil {
		logFatalf("error validating config: %v", err)
	}
}

package config

import (
	"errors"
	"os"
	"reflect"
	"testing"
)

func TestMain(m *testing.M) {
	code := m.Run() // Run all the tests
	os.Exit(code)   // DONE-ZO
}

func TestValidConfig(t *testing.T) {
	cfg := Config{
		KarmaStoreFile: "/tmp/karma",
		LogFile:        "/tmp/log",
		DevTeam:        "teamname",
		DevChannel:     "channelname",
	}

	cfg.Validate()
}

func TestMissingKarmaFile(t *testing.T) {
	oldLogFatalf := logFatalf
	defer func() { logFatalf = oldLogFatalf }()

	var gotFormat string
	var gotV []interface{}
	myFatalf := func(format string, v ...interface{}) {
		gotFormat, gotV = format, v
	}

	logFatalf = myFatalf
	cfg := Config{
		DevTeam:    "teamname",
		DevChannel: "channelname",
		LogFile:    "/log/file",
	}
	cfg.Validate()

	expFormat, expV := "error validating config: %v", []interface{}{errors.New("Missing KarmaStoreFile config value")}
	if gotFormat != expFormat || !reflect.DeepEqual(gotV, expV) {
		t.Errorf("expected error, received: %v %v", gotFormat, gotV)
	}
}

func TestMissingDevTeam(t *testing.T) {
	oldLogFatalf := logFatalf
	defer func() { logFatalf = oldLogFatalf }()

	var gotFormat string
	var gotV []interface{}
	myFatalf := func(format string, v ...interface{}) {
		gotFormat, gotV = format, v
	}

	logFatalf = myFatalf
	cfg := Config{
		DevChannel:     "channelname",
		LogFile:        "/log/file",
		KarmaStoreFile: "/karma/file",
	}
	cfg.Validate()

	expFormat, expV := "error validating config: %v", []interface{}{errors.New("Missing DevTeam config value")}
	if gotFormat != expFormat || !reflect.DeepEqual(gotV, expV) {
		t.Errorf("expected error, received: %v %v", gotFormat, gotV)
	}
}

func TestMissingDevChannel(t *testing.T) {
	oldLogFatalf := logFatalf
	defer func() { logFatalf = oldLogFatalf }()

	var gotFormat string
	var gotV []interface{}
	myFatalf := func(format string, v ...interface{}) {
		gotFormat, gotV = format, v
	}

	logFatalf = myFatalf
	cfg := Config{
		DevTeam:        "teamname",
		LogFile:        "/log/file",
		KarmaStoreFile: "/tmp/karma",
	}
	cfg.Validate()

	expFormat, expV := "error validating config: %v", []interface{}{errors.New("Missing DevChannel config value")}
	if gotFormat != expFormat || !reflect.DeepEqual(gotV, expV) {
		t.Errorf("expected error, received: %v %v", gotFormat, gotV)
	}
}

func TestMissingLogFile(t *testing.T) {
	oldLogFatalf := logFatalf
	defer func() { logFatalf = oldLogFatalf }()

	var gotFormat string
	var gotV []interface{}
	myFatalf := func(format string, v ...interface{}) {
		gotFormat, gotV = format, v
	}

	logFatalf = myFatalf
	cfg := Config{
		KarmaStoreFile: "/tmp/karma",
		DevTeam:        "teamname",
		DevChannel:     "channelname",
	}

	cfg.Validate()
	expFormat, expV := "error validating config: %v", []interface{}{errors.New("Missing LogFile config value")}
	if gotFormat != expFormat || !reflect.DeepEqual(gotV, expV) {
		t.Errorf("expected error, received: %v %v", gotFormat, gotV)
	}

}

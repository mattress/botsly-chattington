package karma

import (
	"github.com/golang/mock/gomock"
	"github.com/keybase/go-keybase-chat-bot/kbchat"
	"gitlab.com/mattress/botsly-chattington/mockapi"
	"os"
	"strings"
	"testing"
)

func TestMain(m *testing.M) {
	code := m.Run() // Run all the tests
	os.Exit(code)   // DONE-ZO
}

func TestReality(t *testing.T) {
	if 1 == 2 {
		t.Errorf("The fabric of the universe has torn. Expected: 1==1, but 1==2")
	}
}

func TestKarmaInit(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)
	ks := NewKarma(kbc, "/fake")
	err := ks.LoadKarmaData(strings.NewReader(`{"mattress": 88, "username": 0}`))
	if err != nil {
		t.Errorf("failed loading karma data: %s ", err.Error())
	}
	k := ks.GetKarma("mattress")
	if k != 88 {
		t.Errorf("karma map empty, expected %d, actual: %d", 88, k)
	}
}

func TestKarmaShutdown(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)
	ks := NewKarma(kbc, "/fake")
	err := ks.LoadKarmaData(strings.NewReader(`{"mattress": 88, "username": 0}`))
	if err != nil {
		t.Errorf("failed loading karma data: %s ", err.Error())
	}

	// TODO FIXME This needs to be fixed, gotta refactor so persist() doesnt fail
	//err = ks.Shutdown()
	//if err != nil {
	//	t.Errorf("shutdown errored out %s", err.Error())
	//}
}

func TestKarmaNewUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)
	ks := NewKarma(kbc, "/fake")
	err := ks.LoadKarmaData(strings.NewReader(`{"mattress": 88, "username": 0}`))
	if err != nil {
		t.Errorf("failed loading karma data: %s ", err.Error())
	}
	tn := "teamname"
	cn := "channelname"
	cmds := []struct {
		user     string
		cmd      string
		timesRun int
		expected string
	}{
		{"newuser", "@newuser+++++++++++++++++++++", 1, "@newuser now has 20 internet points"},
		{"newuser2", "@newuser2---", 1, "@newuser2 now has -2 internet points"},
	}

	for _, cmd := range cmds {
		kbc.EXPECT().SendMessageByTeamName(tn, cmd.expected, &cn).Return(nil).Times(cmd.timesRun)
		_ = ks.KarmaListener(newSubscriptionMsg(tn, cmd.cmd, cn))
	}

	resp := `Karma List (User / #)
0.  mattress  /  88 
1.  newuser  /  20 
2.  username  /  0 
3.  newuser2  /  -2 
`
	kbc.EXPECT().SendMessageByTeamName(tn, resp, &cn).Return(nil).Times(1)
	_ = ks.KarmaListener(newSubscriptionMsg(tn, "/karma", cn))
}

func TestKarmaList(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)
	ks := NewKarma(kbc, "/fake")
	err := ks.LoadKarmaData(strings.NewReader(`{"mattress": 88, "username": 0}`))
	if err != nil {
		t.Errorf("failed loading karma data: %s ", err.Error())
	}
	tn := "teamname"
	cn := "channelname"
	resp := `Karma List (User / #)
0.  mattress  /  88 
1.  username  /  0 
`
	cmds := []struct {
		name        string
		expected    string
		timesRun    int
		isProcessed bool
	}{
		{"/karma", resp, 1, true},
		{"/ksarmachameleon", "", 0, false},
	}
	for _, cmd := range cmds {
		kbc.EXPECT().SendMessageByTeamName(tn, cmd.expected, &cn).Return(nil).Times(cmd.timesRun)
		_ = ks.KarmaListener(newSubscriptionMsg(tn, cmd.name, cn))
	}
}

func TestKarmaModification(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)
	ks := NewKarma(kbc, "/fake")
	err := ks.LoadKarmaData(strings.NewReader(`{"mattress": 88, "username": 0}`))
	if err != nil {
		t.Errorf("failed loading karma data: %s ", err.Error())
	}

	tn := "teamname"
	cn := "channelname"
	testMsgs := []struct {
		username string
		team     string
		channel  string
		cmd      string
		expected string
	}{
		{"username", tn, cn, "@username++++++", "@username now has 5 internet points"},
		{"mattress", tn, cn, "@mattress-----------", "@mattress now has 78 internet points"},
	}

	for _, msg := range testMsgs {
		kbc.EXPECT().SendMessageByTeamName(msg.team, msg.expected, &msg.channel).Return(nil).Times(1)
		isProcessed := ks.KarmaListener(newSubscriptionMsg(msg.team, msg.cmd, msg.channel))
		if !isProcessed {
			t.Errorf("expected cmd to be processed but it wasnt")
		}
	}

	users := []struct {
		name     string
		expected int
	}{
		{"username", 5},
		{"mattress", 78},
	}
	for _, user := range users {
		count := ks.GetKarma(user.name)
		if count != user.expected {
			t.Errorf("expected karma count for %s, expected: %d, actual: %d ", "username", user.expected, count)
		}
	}
}

// newSubscriptionMsg wraps creating new kbchat.SubscriptionMessages
func newSubscriptionMsg(team, body, channel string) kbchat.SubscriptionMessage {
	msg := kbchat.SubscriptionMessage{
		Message: kbchat.Message{
			Content: kbchat.Content{
				Text: kbchat.Text{
					Body: body,
				},
			},
			Channel: kbchat.Channel{
				Name:      team,
				TopicName: channel,
			},
		},
		Conversation: kbchat.Conversation{
			Channel: kbchat.Channel{
				Name:      team,
				TopicName: channel,
			},
		},
	}
	return msg
}

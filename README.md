# Botsly Chattington III, Esq.  
[![Build Status](https://gitlab.com/mattress/botsly-chattington/badges/master/build.svg)](https://gitlab.com/mattress/botsly-chattington/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/mattress/botsly-chattington)](https://goreportcard.com/report/gitlab.com/mattress/botsly-chattington) [![Coverage](https://gitlab.com/mattress/botsly-chattington/badges/master/coverage.svg)](https://gitlab.com/mattress/botsly-chattington/commits/master)  

A chatbot for keybase, written in Go. This is built on top of [github.com/keybase/go-keybase-chat-bot](https://github.com/keybase/go-keybase-chat-bot), which is an experimental side project so note that both of these projects are subject to sudden change.  

### Botsly requires a real keybase user to function.

## Running
```
# Wherever you are running the bot, use the keybase cli to login as the bot user
keybase login mybotuser

# Once authenticated in keybase, clone this repo, build it, and run
git clone ssh:git@gitlab.com/mattress/botsly-chattington.git
cd botsly-chattington
make test && make lint && make build
# Run the generated binary in debug mode
./botsly -log debug
```

### Configuration
#### User/Directories - need to be setup on the deploy target host
```
# User who will run app deploys/runs the bot and the required files/dirs
# TODO proper provisioning script
useradd -r -m /home/botsly botsly`
mkdir -p /opt/botsly/tmp/giphy
touch /opt/botsly/config.yml
touch /opt/botsly/config.yml
touch /var/log/botsly/botsly.log
chown -R botsly:botsly /opt/botsly/
chown -R botsly:botsly /var/log/botsly
```
#### CI - .gitlab-ci.yml - values stored in Gitlab
```
$BIN_NAME - name of the binary being deployed
$SSH_HOST_KEY - host key for server where botsly is deployed
$DEPLOY_USER - name of the user to deploy with
$SSH_PRIVATE_KEY - private key for $DEPLOY_USER
$TARGET_HOST_URL - fqdn or ip of host to deploy to
```

### Yaml Server Configutation - needs to be setup on deploy target host
/opt/botsly/config.yml is where configuration options for Botsly are stored
```
---
// Required - Team Botsly belongs to
DevTeam: myteam

// Required - Channel Botsly will send status/debug messages to.
DevChannel: mychannel

// Required - default location below
LogFile: /var/log/botsly/botsly.log

// Required - Path to file where karma count is stored, default below
KarmaStoreFile: /opt/botsly/karma.json

// Optional - Required only for /gif command
GiphyAPIKey: 0123456789abcdef
GiphyDLDir: /opt/botsly/tmp/giphy
```

## Developing
Happy to accept contributions,just fork the repo, create a feature/ or bug/ branch and file a PR

### Adding new commands
1. Copy the template from commands/template.go
`cp template.go mynewcmd.go`
2. Change the names from ExampleCmd to what you want to call your command
3. Add you command's logic to HandleCmd(cmd) function
4. Send a response with cmd.SendMessage(...) or cmd.SendAttachment(...)
5. Add a reference to the cmdMap in commads.go NewCommandManager(api) function. The key of the map is they keyword to activate the command
```
// NewCommandManager returns a reference to a a new command object
func NewCommandManager(api *kbchat.API, cfg *config.Config) *CommandManager {
  c := &CommandManager{
    api:      api,
    cmdRegex: regexp.MustCompile("^/([a-z\\?]{1,})\\s{0,}(.*)$"),
    cmdMap: map[string]command{
      "?":             nil,
      "help":          nil,
      "insult":        NewInsults(),
      "teachmesempai": &TeachMeSempai{},
      "gif":           NewGipyCmd(cfg, GiphyAPIKey, cfg.GiphyDLDir),
    }
                  // Add your new command to this map
  },              // Can use function or literal declaration
}
```

## TODO
- Add support for deployment multiple environments
- Ansible/shell scripts to handle provisioning server for bot
- Run botsly in docker
- Possibly replace kbchat

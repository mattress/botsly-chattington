package commands

import (
	"math/rand"
	"strings"
	"time"
)

// Insults contains the list of insults
type Insults struct {
	insults []string
}

// NewInsults returns a reference to a new Insults object and seeds the RNG
func NewInsults() *Insults {
	rand.Seed(time.Now().UTC().UnixNano())
	return &Insults{
		insults: []string{
			"is a buttface who has a butt for a face and a face for a butt",
			"will never amount to anything.",
			"doesn't know how to eat spaghetti without making everyone uncomfortable",
			"still pays for pornography.",
			"is afraid of the 'bagel' setting on toasters.",
			"did 9/11.",
		},
	}
}

// HandleCmd is the handler for Insults
func (i *Insults) HandleCmd(cmd Command) error {
	resp := i.insults[rand.Intn(len(i.insults))]
	target := cmd.CmdArgs[1]

	resp = "@" + strings.Trim(target, "@") + " " + resp
	return cmd.SendMessage(resp)
}

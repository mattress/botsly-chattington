package commands

import (
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/keybase/go-keybase-chat-bot/kbchat"
	"gitlab.com/mattress/botsly-chattington/config"
	"gitlab.com/mattress/botsly-chattington/mockapi"
	"gitlab.com/mattress/botsly-chattington/version"

	"os"
	"testing"
)

func TestMain(m *testing.M) {
	code := m.Run() // Run all the tests
	os.Exit(code)   // DONE-ZO
}

func TestInitCommandManager(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tn := "myteam"
	cn := "my-dev-chan"
	kbc := mockapi.NewMockKbchatapi(ctrl)
	out := fmt.Sprintf("Hello there chaps, Botsly %s at your service!", version.Version)

	kbc.EXPECT().SendMessageByTeamName(tn, out, &cn).Return(nil).Times(1)
	cm := NewCommandManager(kbc, &config.Config{DevTeam: "myteam", DevChannel: "my-dev-chan", GiphyDLDir: "/fake/giphy"})
	if cm == nil {
		t.Errorf("failed creating new command manager")
	}
}

func TestCommandList(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)
	out := fmt.Sprintf("Hello there chaps, Botsly %s at your service!", version.Version)

	cn := "my-dev-chan"
	kbc.EXPECT().SendMessageByTeamName("myteam", out, &cn).Return(nil).Times(1)
	cm := NewCommandManager(kbc, &config.Config{DevTeam: "myteam", DevChannel: "my-dev-chan", GiphyDLDir: "/fake/giphy"})
	cmds := cm.GetCommandList()
	if len(cmds) != 5 {
		t.Errorf("Command map initilized with wrong number of commmands")
	}
}

func TestListenerValidCommand(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)

	tn := "teamname"
	cn := "channelname"
	welcomeMsg := fmt.Sprintf("Hello there chaps, Botsly %s at your service!", version.Version)
	welcomeChan := "my-dev-chan"

	kbc.EXPECT().SendMessageByTeamName("myteam", welcomeMsg, &welcomeChan).Return(nil).Times(1)
	cm := NewCommandManager(kbc, &config.Config{DevTeam: "myteam", DevChannel: "my-dev-chan", GiphyDLDir: "/fake/giphy"})

	cmdList := []struct {
		Cmd      string
		Team     string
		Channel  string
		Expected string
		TimesRan int
	}{
		{"/teachmesempai", tn, cn, "https://i.redd.it/tha4tw69qyo01.jpg", 1},
	}

	for _, cmd := range cmdList {
		kbc.EXPECT().SendMessageByTeamName(cmd.Team, cmd.Expected, &cmd.Channel).Return(nil).Times(cmd.TimesRan)
		if c := cm.CommandListener(newSubscriptionMsg(cmd.Team, cmd.Cmd, cmd.Channel)); c != nil {
			cm.Execute(*c)
		}
	}
}

func TestKbchatApiFailure(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)

	tn := "teamname"
	cn := "channelname"
	welcomeMsg := fmt.Sprintf("Hello there chaps, Botsly %s at your service!", version.Version)
	welcomeChan := "my-dev-chan"

	kbc.EXPECT().SendMessageByTeamName("myteam", welcomeMsg, &welcomeChan).Return(nil).Times(1)
	cm := NewCommandManager(kbc, &config.Config{DevTeam: "myteam", DevChannel: "my-dev-chan", GiphyDLDir: "/fake/giphy"})

	errMsg := errors.New("api failed")
	cmdList := []struct {
		Cmd      string
		Team     string
		Channel  string
		Expected error
		TimesRan int
	}{
		{"/notacmd", tn, cn, errMsg, 1},
		{"/stillnotacmd ", tn, cn, errMsg, 1},
		{"/anothernotacmd", tn, cn, errMsg, 1},
		{"sdsdsd", tn, cn, errMsg, 0},
		{"look at the text!", tn, cn, errMsg, 0},
		{"/?", tn, cn, errMsg, 0},
	}

	for _, cmd := range cmdList {
		api := kbc.EXPECT().SendMessageByTeamName(gomock.Any(), gomock.Any(), gomock.Any())
		api.Return(errMsg).Times(cmd.TimesRan)
		_ = cm.CommandListener(newSubscriptionMsg(cmd.Team, cmd.Cmd, cmd.Channel))
	}
}

func TestListenerInvalidCommand(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)

	tn := "teamname"
	cn := "channelname"
	welcomeMsg := fmt.Sprintf("Hello there chaps, Botsly %s at your service!", version.Version)
	welcomeChan := "my-dev-chan"

	kbc.EXPECT().SendMessageByTeamName("myteam", welcomeMsg, &welcomeChan).Return(nil).Times(1)
	cm := NewCommandManager(kbc, &config.Config{DevTeam: "myteam", DevChannel: "my-dev-chan", GiphyDLDir: "/fake/giphy"})

	errMsg := "u wot m8???? ('/?' for help)"
	cmdList := []struct {
		Cmd      string
		Team     string
		Channel  string
		Expected string
		TimesRan int
	}{
		{"/notacmd ", tn, cn, errMsg, 1},
		{"/teachmesempai ", tn, cn, "https://i.redd.it/tha4tw69qyo01.jpg", 1},
		{"/stillnotacmd ", tn, cn, errMsg, 1},
		{"/anothernotacmd", tn, cn, errMsg, 1},
	}

	for _, cmd := range cmdList {
		kbc.EXPECT().SendMessageByTeamName(cmd.Team, cmd.Expected, &cmd.Channel).Return(nil).Times(cmd.TimesRan)
		if c := cm.CommandListener(newSubscriptionMsg(cmd.Team, cmd.Cmd, cmd.Channel)); c != nil {
			cm.Execute(*c)
		}
	}
}

func TestSendMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)

	tn := "teamname"
	cn := "channelname"
	out := "my very fancy output msg"
	cmd := &Command{
		Command: "/mycmd",
		CmdArgs: []string{""},
		Channel: cn,
		Team:    tn,
		api:     kbc,
	}

	kbc.EXPECT().SendMessageByTeamName(cmd.Team, out, &cmd.Channel).Return(nil).Times(1)
	cmd.SendMessage(out)
}

func TestSendAttachment(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	kbc := mockapi.NewMockKbchatapi(ctrl)

	tn := "teamname"
	cn := "channelname"
	f := "/file/path.jpg"
	ti := "title"
	cmd := &Command{
		Command: "/mycmd",
		CmdArgs: []string{""},
		Channel: cn,
		Team:    tn,
		api:     kbc,
	}

	kbc.EXPECT().SendAttachmentByTeam(cmd.Team, f, ti, &cmd.Channel)
	cmd.SendAttachment(f, ti)
}

// newSubscriptionMsg wraps creating new kbchat.SubscriptionMessages
func newSubscriptionMsg(team, body, channel string) kbchat.SubscriptionMessage {
	msg := kbchat.SubscriptionMessage{
		Message: kbchat.Message{
			Content: kbchat.Content{
				Text: kbchat.Text{
					Body: body,
				},
			},
			Channel: kbchat.Channel{
				Name:      team,
				TopicName: channel,
			},
		},
		Conversation: kbchat.Conversation{
			Channel: kbchat.Channel{
				Name:      team,
				TopicName: channel,
			},
		},
	}
	return msg
}

// Package commands is used to listen for and act on commands issued by keybase users
package commands

import (
	"fmt"
	"github.com/keybase/go-keybase-chat-bot/kbchat"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mattress/botsly-chattington/api"
	"gitlab.com/mattress/botsly-chattington/config"
	"gitlab.com/mattress/botsly-chattington/version"
	"regexp"
)

// CommandManager is an object for managing the bots various commands
// Maintains a reference to the keybase api, as well as a map of available commands.
type CommandManager struct {
	api      api.Kbchatapi
	cmdMap   map[string]command
	cmdRegex *regexp.Regexp
	config   *config.Config
}

// Command is instantiated when a PrefixMatch is detected. It contains information
// about the command to be run and arguments for the command
type Command struct {
	CmdArgs []string
	Command string
	Channel string
	Team    string
	api     api.Kbchatapi
}

// SendMessage sends a text reply to the channel/team the command was issued from
// - m - message to send
func (c *Command) SendMessage(m string) error {
	if err := c.api.SendMessageByTeamName(c.Team, m, &c.Channel); err != nil {
		log.WithFields(log.Fields{
			"err": err.Error(),
		}).Error("error sending msg")
		return err
	}
	return nil
}

// SendAttachment uploads an attachment to the channel/team the command was issued from
// - f - path to image file
// - t - title to use for image
func (c *Command) SendAttachment(f string, t string) error {
	if err := c.api.SendAttachmentByTeam(c.Team, f, t, &c.Channel); err != nil {
		log.WithFields(log.Fields{
			"err":     err.Error(),
			"channel": c.Channel,
			"team":    c.Team,
			"file":    f,
		}).Error("error sending attachment")
		return err
	}
	return nil
}

// interface for the handler for a command
type command interface {
	HandleCmd(Command) error
}

// NewCommandManager returns a reference to a a new commandmanager.
func NewCommandManager(api api.Kbchatapi, config *config.Config) *CommandManager {
	c := &CommandManager{
		api:      api,
		cmdRegex: regexp.MustCompile("^/([a-z\\?]{1,})\\s{0,}(.*)$"),
		cmdMap: map[string]command{
			"?":             nil,
			"help":          nil,
			"insult":        NewInsults(),
			"teachmesempai": &TeachMeSempai{},
			"gif":           NewGiphyCmd(config.GiphyAPIKey, config.GiphyDLDir),
		},
		config: config,
	}
	cl := NewHelpCmd(c.GetCommandList())
	c.cmdMap["help"] = cl
	c.cmdMap["?"] = cl
	c.sendOnlineConfirmation(config.DevTeam, config.DevChannel)
	return c
}

func (c *CommandManager) sendOnlineConfirmation(t, ch string) {
	m := fmt.Sprintf("Hello there chaps, Botsly %s at your service!", version.Version)
	if err := c.api.SendMessageByTeamName(t, m, &ch); err != nil {
		log.WithFields(log.Fields{
			"err":     err.Error(),
			"channel": ch,
			"team":    t,
		}).Fatal("error sending welcome msg")
	}
}

// GetCommandList returns a list of available bot commands
func (c *CommandManager) GetCommandList() []string {
	var cmds []string
	for k := range c.cmdMap {
		cmds = append(cmds, k)
	}
	return cmds
}

// CommandListener checks if incoming messages begins with a slash "/", and if
// so creates a Command object using the arguments from the msg
func (c *CommandManager) CommandListener(msg kbchat.SubscriptionMessage) *Command {
	body := msg.Message.Content.Text.Body
	team := msg.Conversation.Channel.Name
	channel := msg.Conversation.Channel.TopicName
	sender := msg.Message.Sender.Username

	matches := c.cmdRegex.FindAllStringSubmatch(body, -1)
	log.WithFields(log.Fields{
		"matches": matches,
		"sender":  sender,
		"channel": channel,
		"team":    team,
		"cmd":     body,
	}).Debug("Command Listener Matches")

	if len(matches) < 1 {
		log.WithFields(log.Fields{
			"sender": sender,
			"cmd":    body,
		}).Debug("no command detected")
		return nil
	}

	cmd := matches[0][1]
	args := matches[0][1:]

	if _, ok := c.cmdMap[cmd]; !ok {
		if err := c.api.SendMessageByTeamName(team, "u wot m8???? ('/?' for help)", &channel); err != nil {
			log.WithFields(log.Fields{
				"sender": sender,
				"body":   body,
				"dest":   channel,
				"err":    err.Error(),
			}).Error("failed sending bad command response")
			return nil
		}
		return nil
	}

	return &Command{
		Command: cmd,
		CmdArgs: args,
		Channel: channel,
		Team:    team,
		api:     c.api,
	}
}

// Execute triggers the handler for an event
func (c *CommandManager) Execute(e Command) error {
	return c.cmdMap[e.Command].HandleCmd(e)
}

package commands

// ExampleCmd is an example of how to add a command to this project
// Make sure to changes these to more relevant comments
// This can be empty or populated with values to be used in the handler and any
// other helper functions
type ExampleCmd struct{}

// HandleCmd is the handler for ExampleCmd
// Make sure to change the name of the reference to ExampleCmd below and also this comment
//  - cmd will contain the command, arguments, team and channnel info
func (e *ExampleCmd) HandleCmd(cmd Command) error {
	// Do stuff here
	resp := "OMG NEW MESSAGE"
	return cmd.SendMessage(resp)
	// return cmd.SendAttachment("filepath", "title")
}
